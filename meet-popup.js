javascript:(function () {

    let w = 1000;
    let h = 800;
    let t = 0;
    let l = (screen.width - w) / 2;

    if (window.location.origin.indexOf("meet.google.com") === -1) {
        console.error('Not in meet.google.com', window.location.origin);
        return;
    }

    let url = window.location.href;
    let name = "meet-popup";
    let options = "" +
        "location=no," +
        "directories=no," +
        "menubar=no," +
        "toolbar=no," +
        "scrollbars=no," +
        "status=no," +
        "resizable=yes," +
        "width=" + w + "," +
        "height=" + h + "," +
        "left=" + l + "," +
        "top=" + t;

    console.debug('url', url);
    console.debug('name', name);
    console.debug('options', options);

    let popup = window.open(url, name, options);
    if (!popup) {
        console.error('Popup failed');
        return;
    }
    
    console.debug('Popup Ok', popup);
    let limit = 100;
    let timer = setInterval(()=> {
        if (popup.document.title.indexOf("Popup") === -1) {
            console.debug(popup.document.title);
            popup.document.title = "Popup " + popup.document.title;
        }
        if (popup.document.title === "Popup " + window.document.title) {
            clearInterval(timer);
            close();
        }
        if (--limit < 0) {
            clearInterval(timer);
            close();
        }
    }, 100);

}());
