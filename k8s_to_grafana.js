javascript:(function () {

    const k8s_ns = 'KpOGObzWk';
    const k8s_ip = "10.26.12.10";
    const k8s_port = "9090";
    const grafana_port = "89";

    if (window.location.origin.indexOf(k8s_ip + ':' + k8s_port) === -1) return;

    const origin = window.location.href;
    const match = origin.match(/\/(?<pod_id>(\w+-){2,}\w+)[\/?]/i);
    const pod = match.groups.pod_id;
    const template = "http://" + k8s_ip + ":" + grafana_port + "/d/" + k8s_ns + "/pods?var-pod={{POD}}";
    const target = template.replace(/{{POD}}/g, pod);

    window.location.href = target;
    return false;
}());
