javascript:(function () {

    let w = 1000;
    let h = 80;
    let t = 0;
    let l = (screen.width - w) / 2;

    if (window.location.origin.indexOf("www.deezer.com") === -1) {
        window.open('https://www.deezer.com');
        return;
    }

    if (null === dzPlayer) {
        console.error('dzPlayer not found');
        return;
    }

    let index = dzPlayer.getIndexSong();
    console.debug('index', index);
    let offset = Math.floor(dzPlayer.getPosition());
    console.debug('offset', offset);

    let url = window.location.origin + window.location.pathname + "/?autoplay=true";
    let name = "deezer-popup";
    let options = "location=no," +
        "directories=no," +
        "menubar=no," +
        "toolbar=no," +
        "scrollbars=no," +
        "status=no," +
        "resizable=yes," +
        "width=" + w + "," +
        "height=" + h + "," +
        "left=" + l + "," +
        "top=" + t;

    console.debug('url', url);
    console.debug('name', name);
    console.debug('options', options);

    if (window.open(url, name, options)) {
        dzPlayer.control.pause();
        close();
    }

}());