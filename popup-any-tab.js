javascript:(function () {

    let w = 320;
    let h = 180;
    let t = 0;
    let l = (screen.width - w) / 2;

    let url = window.location.href;
    let name = url;
    let options = "location=no," +
        "directories=no," +
        "menubar=no," +
        "toolbar=no," +
        "scrollbars=no," +
        "status=no," +
        "resizable=yes," +
        "width=" + w + "," +
        "height=" + h + "," +
        "left=" + l + "," +
        "top=" + t;

    console.debug('url', url);
    console.debug('name', name);
    console.debug('options', options);

    if (window.open(url, name, options)) {
        close();
    }

}());