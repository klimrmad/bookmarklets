javascript:(function () {

    let w = 320;
    let h = 180;
    let t = 0;
    let l = (screen.width - w) / 2;

    if (window.location.origin.indexOf("www.youtube.com") === -1) {
        window.open('https://youtube.com');
        return;
    }

    let player = document.getElementById('movie_player');
    if (null === player) {
        console.error('Youtube player not found');
        return;
    }

    let start = Math.floor(player.getCurrentTime());
    console.debug('start', start);
    let vdata = player.getVideoData();
    console.debug('vdata', vdata);
    let vid = vdata.video_id;

    let url = "http://www.youtube.com/embed/" + vid + "/?autoplay=1&start=" + start;
    let name = "youtube-popup";
    let options = "location=no," +
        "directories=no," +
        "menubar=no," +
        "toolbar=no," +
        "scrollbars=no," +
        "status=no," +
        "resizable=yes," +
        "width=" + w + "," +
        "height=" + h + "," +
        "left=" + l + "," +
        "top=" + t;

    console.debug('url', url);
    console.debug('name', name);
    console.debug('options', options);

    if (window.open(url, name, options)) {
        player.pauseVideo();
        close();
    }

}());