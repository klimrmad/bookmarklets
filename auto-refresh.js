javascript:(function () {

    let timeout = prompt("Set timeout [s]");
    if (timeout > 0) {
        let current = location.href;

        function reload() {
            with (document) {
                write('<frameset cols="*"><frame src="' + current + '"/></frameset>');
                void (close())
            }
        }

        setInterval(reload, 1000 * timeout);
    }

}());
