javascript:(function () {

    const k8s_ip = "10.26.12.10";
    const k8s_port = "9090";
    const kibana_port = "88";

    if (window.location.origin.indexOf(k8s_ip + ':' + k8s_port) === -1) return;

    const origin = window.location.href;
    const match = origin.match(/\/(?<pod_id>(\w+-){2,}\w+)[\/?]/i);
    const pod = match.groups.pod_id;
    const template = "http://" + k8s_ip + ":" + kibana_port + "/app/kibana#/discover/?_g=(filters:!(),refreshInterval:('$$hashKey':'object:2052',display:'5 seconds',pause:!f,section:1,value:5000),time:(from:now-24h,mode:quick,to:now))&_a=(columns:!(log,kubernetes.container_name),filters:!(('$state':(store:appState),meta:(alias:{{POD}},disabled:!f,index:'83721730-a54c-11e8-87d6-d705c09cade9',key:kubernetes.pod_name,negate:!f,params:(query:{{POD}},type:phrase),type:phrase,value:{{POD}}),query:(match:(kubernetes.pod_name:(query:{{POD}},type:phrase))))),index:'83721730-a54c-11e8-87d6-d705c09cade9',interval:auto,query:(language:lucene,query:''),sort:!('@timestamp',desc))";
    const target = template.replace(/{{POD}}/g, pod);

    window.location.href = target;
    return false;
}());
